package be.familieheist.web.content.item;

public interface ItemDataDBO {
    public ItemDataDTO toDto();
}
